﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Class.aspx.cs" Inherits="_Class" MasterPageFile="~/MasterPage.master" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<!---banner--->
			<div class="banner wow fadeInDownBig animated animated" data-wow-delay="0.4s">
				<div class="container">
					<h2>Услуги</h2>
				</div>
			</div>
			<!---banner--->
			
		<!---class--->
			<div class="class">
				<div class="container">
					<div class="class-grids w3l">
						<div class="col-md-8 class-grid wow fadeInLeft animated animated" data-wow-delay="0.4s">
							<div class="class-top">
								<div class="col-md-6 class-left">
									<img src="images/c1.jpg" class="img-responsive" alt=""/>
									<div class="class-text  hvr-bounce-to-bottom">
										<h4>Зал для бокса и борьбы </h4>
										<p>Занятие боксом – прекрасное средство для обретения спортивного, тренированного, выносливого и гибкого тела.Занятия борьбой развивают силу, выносливость, ловкость, быстроту реакции, координацию движений и чувство равновесия. </p>
										<h5></h5>
										<p></p>
										<h5></h5>
										<p></p>
									</div>
								</div>
								<div class="col-md-6 class-right">
								<img src="images/c2.jpg" class="img-responsive" alt=""/>
									<div class="class-text hvr-bounce-to-bottom">
										<h4>Тренажерный зал </h4>
										<p>Занятия в тренажерном зале позволят изменить форму тела,вес и надолго закрепить достигнутый результат. </p>
										<h5></h5>
										<p></p>
										<h5></h5>
										<p></p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="class-top top2 w3l-agile">
								<div class="col-md-6 class-left">
									<img src="images/c3.jpg" class="img-responsive" alt=""/>
									<div class="class-text hvr-bounce-to-bottom">
										<h4>Зал для тенниса </h4>
										<p>Зал настольного тенниса оборудован игровыми столами и инвентарем, позволяющим проводить турниры в одиночных и парных состязаниях различного уровня. </p>
										<h5></h5>
										<p></p>
										<h5></h5>
										<p></p>
									</div>
								</div>
								<div class="col-md-6 class-right">
								<img src="images/c5.jpg" class="img-responsive" alt=""/>
									<div class="class-text hvr-bounce-to-bottom">
										<h4>Зал для хоккея </h4>
										<p>Зал для тренировок хоккеистов оснащен соответствующим спортивным инвентарем и оборудованием. </p>
										<h5></h5>
										<p></p>
										<h5></h5>
										<p></p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-4 class-grid1 wow fadeInRight animated animated" data-wow-delay="0.4s">
							<div class="recent-top w3l">
								<h4>Залы</h4>
								<div class="recent-class">
									<div class="recent-left">
										<img src="images/r1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="recent-right">
										<h5>Силовые тренажеры</h5>
										<p><strong></strong></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="recent-class">
									<div class="recent-left">
									<img src="images/r2.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="recent-right">
										<h5>Беговые дорожки</h5>
										<p><strong></strong></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="recent-class">
									<div class="recent-left">
									<img src="images/r3.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="recent-right">
										<h5>Большой тренажерный зал</h5>
										<p><strong></strong></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="register">
								<h4>Заявка на аренду</h4>
								<form action="#" method="post">
									<input type="text" name="name" placeholder="Name" required="">
									<input type="text" name="email" class="email1" placeholder="Email" required="">
									<input type="text" name="phone" placeholder="Phone" required="">
									<input type="submit" value="отправить">
								</form>
							</div>
							<div class="more-class">
								<img src="images/p4.jpg" class="img-responsive" alt=""/>
							</div>
							<div class="more-class">
								<img src="images/p5.jpg" class="img-responsive" alt=""/>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		<!---class--->
</asp:Content>
