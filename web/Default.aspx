﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" MasterPageFile="~/MasterPage.master" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<!---banner--->
	<div class="banner-section">
		<div class="container">
			<div class="slider">
				<div class="callbacks_container">
					<ul class="rslides" id="slider">
						 <li>	 
							<div class="caption">
							<div class="col-md-6 cap-left wow fadeInDownBig animated animated" data-wow-delay="0.4s">
								<p>Занятия в нашем спортивно-оздоровительном комплексе даст возможность эффективно развивать такие физические качества как: быстрота, выносливость, гибкость, сила, координацию движений.  </p>
					
							</div>
							<div class="col-md-6 cap-right wow fadeInUpBig animated animated" data-wow-delay="0.4s">
								<h3>Быть первым, быть лучшим!</h3>
							</div>
							<div class="clearfix"></div>
							</div>
						</li>
						 <li>	 
							<div class="caption">
							<div class="col-md-6 cap-left wow fadeInDownBig animated animated" data-wow-delay="0.4s">
								<p> Занятия в нашем спортивно-оздоровительном комплексе поможет овладеть физическими навыками во многих видах спорта. </p>
								
							</div>
							<div class="col-md-6 cap-right wow fadeInUpBig animated animated" data-wow-delay="0.4s">
								<h3>Дружно, смело с оптимизмом-за здоровый образ жизни!</h3>
							</div>
								<div class="clearfix"></div>
							</div>
							
						 </li>
						<li>	 
							<div class="caption">
							<div class="col-md-6 cap-left wow fadeInDownBig animated animated" data-wow-delay="0.4s">
								<p>Занятия в нашем спортивно-оздоровительном комплексе поднимет настроение, укрепит мышцы, повысит уверенность в себе.</p>
								
							</div>
							<div class="col-md-6 cap-right wow fadeInUpBig animated animated" data-wow-delay="0.4s">
								<h3>Максимум спорта и смеха-с крепким здоровьем добьемся успеха!</h3>
							</div>
							<div class="clearfix"></div>
							</div>
							
						 </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<!---banner--->
			<div class="content">
				<!---train--->
					<div class="train w3-agile">
						<div class="container">
							<h2>Тренируйся у нас</h2>
							<div class="train-grids">
								<div class="col-md-3 train-grid wow fadeInLeft animated animated" data-wow-delay="0.4s">
									<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e1.png"/>
										</div>
										<h4></h4>
										<p></p>
									</div>
								</div>
								<div class="col-md-3 train-grid wow fadeInDownBig animated animated" data-wow-delay="0.4s">
									<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e2.png"/>
										</div>
										<h4></h4>
										<p></p>
									</div>
								</div>
								<div class="col-md-3 train-grid wow fadeInUpBig animated animated" data-wow-delay="0.4s">
									<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e3.png"/>
										</div>
										<h4> </h4>
										<p></p>
									</div>
								</div>
								<div class="col-md-3 train-grid wow fadeInRight animated animated" data-wow-delay="0.4s">
								<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e4.png"/>
										</div>
										<h4></h4>
										<p></p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="train-grids">
								<div class="col-md-3 train-grid wow fadeInLeft animated animated" data-wow-delay="0.4s">
									<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e5.png"/>
										</div>
										<h4></h4>
										<p></p>
									</div>
								</div>
								<div class="col-md-3 train-grid wow fadeInDownBig animated animated" data-wow-delay="0.4s">
									<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e6.png"/>
										</div>
										<h4></h4>
										<p></p>
									</div>
								</div>
								<div class="col-md-3 train-grid wow fadeInUpBig animated animated" data-wow-delay="0.4s">
									<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e7.png"/>
										</div>
										<h4> </h4>
										<p></p>
									</div>
								</div>
								<div class="col-md-3 train-grid wow fadeInRight animated animated" data-wow-delay="0.4s">
									<div class="train-top hvr-bounce-to-right">
										<div class="train-img">
											<img src="images/e8.png"/>
										</div>
										<h4> </h4>
										<p></p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				<!---train--->
				<div class="fit-section w3l-layouts">
					<div class="container">
						<div class="fit-grids">
							<div class="col-md-4 fit-grid wow fadeInDownBig animated animated" data-wow-delay="0.4s">
								<div class="fit-left hvr-bounce-to-bottom">
									<h3>ЗАЛ ДЛЯ БОКСА</h3>
									<p>Занятие боксом – прекрасное средство для обретения спортивного, тренированного, выносливого и гибкого тела.</p>
									<ul>
									<li></li>
									<li></li>
									<li></li>
									</ul>
								</div>
							</div>
							<div class="col-md-4 fit-grid wow fadeInUpBig animated animated" data-wow-delay="0.4s">
								<img src="images/f2.jpg" class="img-responsive" alt=""/>
							</div>
							<div class="col-md-4 fit-grid wow fadeInRight animated animated" data-wow-delay="0.4s">
								<div class="fit-right hvr-bounce-to-right">
									<h3>ТРЕНАЖЕРНЫЙ ЗАЛ</h3>
									<p> Занятия в тренажерном зале позволят изменить форму тела,вес и надолго закрепить достигнутый результат.  </p>
									
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="fit-grids1">
							<div class="col-md-8 fit-grid2 wow fadeInLeft animated animated" data-wow-delay="0.4s">
								<img src="images/f3.jpg" class="img-responsive" alt=""/>
							</div>
							<div class="col-md-4 fit-grid1 hvr-bounce-to-bottom wow fadeInDownBig animated animated" data-wow-delay="0.4s">
								<div class="fit-right">
									<h3>САУНА</h3>
									<p>Прогревание в сауне – отличное средство восстановления после спортивных тренировок. Здесь всегда можно спокойно отдохнуть и расслабиться.</p>
									
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="fit-grids wow fadeInLeft animated animated" data-wow-delay="0.4s">
						<div class="col-md-4 fit-grid">
								<img src="images/f1.jpg" class="img-responsive" alt=""/>
							</div>
							<div class="col-md-4 fit-grid wow fadeInDownBig animated animated" data-wow-delay="0.4s">
								<div class="fit-left hvr-bounce-to-bottom">
									<h3>ЗАЛ ДЛЯ ТЕННИСА</h3>
									<p>Зал настольного тенниса оборудован игровыми столами и инвентарем, позволяющим проводить турниры в одиночных и парных состязаниях различного уровня.</p>
									<ul>
									<li></li>
									<li></li>
									<li></li>
									</ul>
								</div>
							</div>
							<div class="col-md-4 fit-grid wow fadeInRight animated animated" data-wow-delay="0.4s">
								<div class="fit-right hvr-bounce-to-right">
									<h3>ЗАЛ ДЛЯ БОРЬБЫ</h3>
									<p>Занятия борьбой развивают силу, выносливость, ловкость, быстроту реакции, координацию движений и чувство равновесия. </p>
									
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="fit-grids1">
						<div class="col-md-4 fit-grid1 hvr-bounce-to-bottom wow fadeInDownBig animated animated" data-wow-delay="0.4s">
								<div class="fit-right">
									<h3>ХОККЕЙ</h3>
									<p>Зал для тренировок хоккеистов оснащен соответствующим спортивным инвентарем и оборудованием.  </p>
									
								</div>
							</div>
							<div class="col-md-8 fit-grid2 wow fadeInUpBig animated animated" data-wow-delay="0.4s">
								<img src="images/f4.jpg" class="img-responsive" alt=""/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<!---Benefits - Join Today!--->
					<div class="benefits w3l">
						<div class="container">
							<div class="benefits-grids">
								<div class="col-md-6 benefits-grid wow fadeInLeft animated animated" data-wow-delay="0.4s">
									<h3>Наши преимущества</h3>
									<div class="benefit-top">
										<div class="benefit-left">
											<h4>1</h4>
										</div>
										<div class="benefit-right">
											<p> Удобное место расположение спортивно-оздоровительного комплекса.</p>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="benefit-top">
										<div class="benefit-left">
											<h4>2</h4>
										</div>
										<div class="benefit-right">
											<p> Приемлемые цены за аренду спортивных залов.</p>
										</div>
										<div class="clearfix"></div>
									</div><div class="benefit-top">
										<div class="benefit-left">
											<h4>3</h4>
										</div>
										<div class="benefit-right">
											<p> Для иногородних размещение на территории комплекса.</p>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="benefit-top">
										<div class="benefit-left">
											<h4>4</h4>
										</div>
										<div class="benefit-right">
											<p> Мы вам всегда рады!</p>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="col-md-6 benefits-grid1 wow fadeInRight animated animated" data-wow-delay="0.4s">
									<form action="#" method="post">
										<input type="text" name="name" placeholder="Name *" required>
										<input type="text" name="email" class="email" placeholder="Email *" required>
										<input type="text" name="phone"  class="phone"placeholder="Phone Number *" required>
										<textarea name="text area" placeholder="Message *"></textarea>
										<input type="submit" value="присоединяйтесь">
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				<!---Benefits - Join Today!--->
			</div>
			<div class="testimonials-section">
			<div class="container">
				<h3>Отзывы</h3>
				<div class="testimonials-grids">
					<div class="col-md-2 testimonials-grid1 test1 wow fadeInDownBig animated animated" data-wow-delay="0.4s">
						<img src="images/t1.png" class="img-responsive" alt=""/>
					</div>
					<div class="col-md-10 testimonials-grid wow fadeInRight animated animated" data-wow-delay="0.4s">
						<p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum. Souvlaki ignitus carborundum e pluribus unum. Defacto lingo est igpay atinlay. Marquee selectus non provisio incongruous feline nolo contendre. Gratuitous octopus niacin, sodium glutimate. Quote meon an estimate et non interruptus stadium.</p>
						<h5>Antonio Brightman</h5>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="testimonials-grids">
					<div class="col-md-10 testimonials-grid wow fadeInUpBig animated animated" data-wow-delay="0.4s">
						<p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum. Souvlaki ignitus carborundum e pluribus unum. Defacto lingo est igpay atinlay. Marquee selectus non provisio incongruous feline nolo contendre. Gratuitous octopus niacin, sodium glutimate. Quote meon an estimate et non interruptus stadium.</p>
						<h5>Brightman</h5>
					</div>
					<div class="col-md-2 testimonials-grid1 test wow fadeInLeft animated animated" data-wow-delay="0.4s">
						<img src="images/t2.png" class="img-responsive" alt=""/>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
</asp:Content>