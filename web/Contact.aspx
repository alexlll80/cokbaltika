﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact"  MasterPageFile="~/MasterPage.master" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
			<div class="banner wow fadeInDownBig animated animated" data-wow-delay="0.4s">
				<div class="container">
					<h2>НАШИ КОНТАКТЫ</h2>
				</div>
			</div>
	<div class="contact w3l">
		<div class="map wow fadeInUpBig animated animated" data-wow-delay="0.4s">
			<iframe src="https://api-maps.yandex.ru/frame/v1/-/CZwWBGkp"></iframe>
		</div>
		<div class="map-grids w3-agile">
			<div class="col-md-8 map-middle wow fadeInRight animated animated" data-wow-delay="0.4s">
				<form action="#" method="post">
										<input type="text" name="name" placeholder="Name" required="">
										<input type="text" name="email" class="email" placeholder="Email" required="">
										
										<textarea name="text area" placeholder="Message"></textarea>
										<input type="submit" value="отправить">
									</form>
			</div>
			<div class="col-md-4 map-left wow fadeInLeft animated animated" data-wow-delay="0.4s">
				<h4>Адрес</h4>
				<ul>
					<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> Барнаул, Космонавтов 34</li>
					<li><i class="glyphicon glyphicon-phone" aria-hidden="true"></i> Телефон: +7 913 215 9632</li>
					<li><i class="glyphicon glyphicon-phone" aria-hidden="true"></i> Телефон: +7 913 215 9632</li>
					<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i> <a href="#"><a href="mailto:info@example.com">пример@почтового.ящика</a></a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</asp:Content>